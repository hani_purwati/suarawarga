<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Auth
Auth::routes();

Route::get('/erd', function () {
    return view('welcome');
});
Route::get('/about', function () {
    return view('about');
});
Route::get('/', 'HomeController@index')->name('home');

// Function untuk membuat semua alamat harus diakses melalui login terlebih dahulu
Route::group(['middleware' => ['auth']], function () {
    
    // Template
    Route::get('/test', function(){
        return view('layouts.master');
    });

    //Ganti Password
    

    Route::get('/password', 'ChangePasswordController@index');
    Route::post('/password', 'ChangePasswordController@store')->name('change.password');
      
    
    // CRUD Kategori Query Builder
    Route::get('/kategori','KategoriController@index');
    Route::get('/kategori/create','KategoriController@create');
    Route::post('/kategori','KategoriController@store');
    Route::get('/kategori/{kategori_id}','KategoriController@show');
    Route::get('/kategori/{kategori_id}/edit','KategoriController@edit');
    Route::put('/kategori/{kategori_id}','KategoriController@update');
    Route::delete('/kategori/{kategori_id}','KategoriController@destroy');
    
    Route::get('/komentar/{kategori_id}/create','KomentarController@create');
    Route::delete('/komentar/{kategori_id}','KomentarController@destroy');
    Route::resource('postingan','PostinganController');


    // CRUD Profile Menggunakan Eloquent ORM
    Route::resource('profil','ProfilController');
    Route::resource('komentar','KomentarController');
    Route::resource('likes','LikesController');
});
    // CRUD Profile Menggunakan Eloquent ORM
    Route::resource('tema','TemaController');


