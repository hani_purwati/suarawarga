## <b> Final Project </b>



# Kelompok 22

1. Rudi Nugroho C.
2. Hani Purwati H.
3. Roby Wijonarko P

# Tema Project

Suara Warga
Merupakan media untuk warga menceritakan, melaporkan, atau mengadukan berbagai hal di lingkungannya. Setiap warga dapat saling berkomentar, dan topik pembicaraan yang mendapat banyak komentar terbaru menjadi trending topik.

# ERD

<p align="center"><img src="public/images/ERD.png"></p>

# Link Video

Link Video Demo Aplikasi :https://youtu.be/z0DuI2NjOwU <br>
Link Deploy : <br>
<ul>
<li> https://git.heroku.com/nameless-mesa-07160.git</li>
<li> http://nameless-mesa-07160.herokuapp.com/profil</li>
</ul>