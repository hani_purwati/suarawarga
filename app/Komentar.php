<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $table = 'komentar';
    protected $fillable = ['tema_id','user_id','komen','foto','parent'];

    public function author(){
        
        return $this->belongsTo('App\User','user_id');
        
    }

    public function tema(){
        
        return $this->belongsTo('App\Tema','tema_id');
        
    }
}
