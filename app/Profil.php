<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $table = 'profil';
    protected $fillable = ['user_id','alamat','umur','foto_profil'];

     //ORM untuk relasi tabel user
     public function user(){
        
        return $this->belongsTo('App\User');
        
    }
}
