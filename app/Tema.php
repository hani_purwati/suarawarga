<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tema extends Model
{
    protected $table = 'tema';
    protected $fillable = ['user_id','kategori_id','judul','deskripsi','foto','status_public'];

    //ORM untuk relasi tabel komentar
    public function komentar(){
        return $this->hasMany('App\Komentar');
    }

    
}
