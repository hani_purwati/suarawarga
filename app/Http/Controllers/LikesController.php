<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Likes; // Dipanggil karena menggunakan model Tema (Eloquent ORM), jangan salah penggunaan 'slash' (\) bukan (/)


class LikesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tema_id' => 'required',
        ]);
        $id = Auth::id();  // mengammbil id user melalui Auth
        // Mencari dulu apakah sudah pernah Likes
        $likes = Likes::where('tema_id',$request->tema_id)
                        ->where('user_id',$id)
                        ->count();
        if ($likes == 0){
            $query = [
                "tema_id" => $request->tema_id,
                "user_id" => $id,
                "like_status" => '1'
            ];
            Likes::create($query);   // Komentar disini disebut karena menggunakan model Likes.php
        }
        return redirect()->route('tema.show', $request->tema_id); //kembali ke halaman route sebelumnya dengan cara masuk ke controller TEma fungsi show
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
