<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Tema; // Dipanggil karena menggunakan model Tema (Eloquent ORM), jangan salah penggunaan 'slash' (\) bukan (/)
use App\Komentar; // Dipanggil karena menggunakan model Komentar (Eloquent ORM), jangan salah penggunaan 'slash' (\) bukan (/)
use File; // Digunakan jika ada pengalamatan file
Use DB;

class KomentarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_user = Auth::id();  // mengammbil id user melalui Auth
        $komenku = Komentar::where('user_id',$id_user)->get();
        return view('komentar.index', compact('komenku'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $tema = Tema::find($id);
        return view('komentar.create', compact('tema')); //untuk menuju ke form create/tambah data komentar tapi sambil mengambil isi tabel kategori
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'tema_id' => 'required',
    		'user_id' => 'required',
    		'komen' => 'required',
    		'foto' => 'mimes:jpeg,png,jpg|max:2200', // mimes ini untuk membatasi file apa saja yang diperbolehkan diupload, max untuk maksimal besar filenya
    		'parent' => 'required'
    	]);

        if ($request->has('foto')) {  // jika foto ditambahkan (user upload file)
            $foto = $request->foto;
            $new_foto = time() . '-' . $foto->getClientOriginalName(); // rename nama file foto
            $foto->move('images/', $new_foto); // menyimpan file yang diupload user pada alamat yang ditentukan, yaitu pada folder public/images
            $komen_data = [   
                'tema_id' => $request->tema_id,
                'user_id' => $request->user_id,
                'komen' => $request->komen,
                'foto' => $new_foto, // nama baru file foto disimpan di tabel database
                'parent' => $request->parent
            ];
        } else {
            $komen_data = [   
                'tema_id' => $request->tema_id,
                'user_id' => $request->user_id,
                'komen' => $request->komen,
                'foto' => 'NULL', // foto kosong
                'parent' => $request->parent
            ];
        }

        Komentar::create($komen_data);   // Komentar disini disebut karena menggunakan model Komentar.php
        $id_tema = $request->tema_id;
        //return redirect('tema.show', compact('tema','kategori','komentar','komen'));
        return redirect()->route('tema.show', $id_tema); //kembali ke halaman route sebelumnya dengan cara masuk ke controller TEma fungsi show
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tema = Tema::find($id);
        return view('komentar.edit', compact('tema')); //untuk menuju ke form create/tambah data komentar tapi sambil mengambil isi tabel kategori
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'tema_id' => 'required',
    		'user_id' => 'required',
    		'komen' => 'required',
    		'foto' => 'mimes:jpeg,png,jpg|max:2200', // mimes ini untuk membatasi file apa saja yang diperbolehkan diupload, max untuk maksimal besar filenya
    		'parent' => 'required'
    	]);

        if ($request->has('foto')) {  // jika foto ditambahkan (user upload file)
            $foto = $request->foto;
            $new_foto = time() . '-' . $foto->getClientOriginalName(); // rename nama file foto
            $foto->move('images/', $new_foto); // menyimpan file yang diupload user pada alamat yang ditentukan, yaitu pada folder public/images
            $komen_data = [   
                'tema_id' => $request->tema_id,
                'user_id' => $request->user_id,
                'komen' => $request->komen,
                'foto' => $new_foto, // nama baru file foto disimpan di tabel database
                'parent' => $request->parent
            ];
        } else {
            $komen_data = [   
                'tema_id' => $request->tema_id,
                'user_id' => $request->user_id,
                'komen' => $request->komen,
                'foto' => 'NULL', // foto kosong
                'parent' => $request->parent
            ];
        }

        Komentar::create($komen_data);   // Komentar disini disebut karena menggunakan model Komentar.php
        $tema = Tema::find($request->tema_id);       
        $kategori = DB::table('kategori')->get(); // mengambil data dari tabel kategori juga, sedangkan pengecekkan kategori_id pada tabel tema dan id pada tabel kategori dilakukan di edit.blade.php
        $komentar = komentar::where('tema_id',$request->tema_id)->count();
        return redirect('tema.show', compact('tema','kategori','komentar'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $komentar = Komentar::findorfail($id);
        $id_tema = $komentar->tema_id;
        if ($komentar->foto == "NULL") {  // jika ada foto pada komentar
            $komentar->delete();
        } else {
            $komentar->delete();        
            $path = 'images/';
            File::delete($path.$komentar->foto);
            }
        return redirect()->route('tema.show', $id_tema);
    }
}
