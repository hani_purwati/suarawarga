<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Profil; // Dipanggil karena menggunakan model Profil (Eloquent ORM), jangan salah penggunaan 'slash' (\) bukan (/)
use App\User; // Model User
use App\Likes; // Model User
use File; // Digunakan jika ada pengalamatan file
Use DB;
use App\Tema; // Dipanggil karena menggunakan model Tema (Eloquent ORM), jangan salah penggunaan 'slash' (\) bukan (/)
use App\Komentar; // Dipanggil karena menggunakan model Komentar (Eloquent ORM), jangan salah penggunaan 'slash' (\) bukan (/)


class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $profil = Profil::all();
        // return view('profil.index', compact('profil'));

        // By Rudi
        $id_profil = Auth::User()->profil->id;
        $profil = Profil::find($id_profil);
        
        $id_user = Auth::id();  // mengammbil id user melalui Auth

        $temaku = Tema::where('user_id',$id_user)->count(); // Menghitung Jumlah Tema yang dibuat user yang login
        $komentarku = Komentar::where('user_id',$id_user)->count(); // Menghitung Jumlah Komentar yang dibuat user yang login
        $likesku = Likes::where('user_id',$id_user)->count(); // Menghitung Jumlah Likes yang dibuat user yang login

        $semua_user = DB::table('users')
                ->join('profil', 'users.id', '=', 'profil.user_id')
                ->select('users.id', 'users.name', 'profil.foto_profil', 'profil.alamat')
                ->get(); // Mengambil semua User dan Profil

        $alluser = User::all();
        $semua_tema = Tema::select('id','user_id','deskripsi')
                ->where('status_public', '1')
                ->orderBy('created_at','DESC')
                ->get();
        // $komentar = DB::table('komentar')
        //         ->selectRaw('count(id) as number_of_komentar, tema_id')
        //         ->groupBy('tema_id')
        //         ->get();

         $komentar = Komentar::selectRaw('tema_id, count(id) as number_of_komentar')
                 ->groupBy('tema_id')
                 ->get();
        

        // $trending = DB::table('komentar')
        //         ->join('tema', 'komentar.tema_id', '=', 'tema.id')
        //         ->selectRaw('count(komentar.id) as number_of_komentar, komentar.tema_id, tema.id, tema.user_id, tema.deskripsi, tema.created_at')
        //         ->groupBy('komentar.tema_id')
        //         ->get();

        $trending = Komentar::join('tema', 'komentar.tema_id', '=', 'tema.id')
                ->selectRaw('komentar.tema_id,count(komentar.id) as number_of_komentar,  tema.id, tema.user_id, tema.deskripsi, tema.created_at')
                ->groupBy('komentar.tema_id','tema.id')
                ->get();

        return view('profil.index', compact('profil','temaku','komentarku','likesku','semua_tema','komentar','semua_user','trending','alluser'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = DB::table('users')->get();
        return view('profil.create', compact('users')); //untuk menuju ke form create/tambah data profil tapi sambil mengambil isi tabel users
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'judul' => 'required',
    		'ringkasan' => 'required',
    		'poster' => 'required|mimes:jpeg,png,jpg|max:2200', // mimes ini untuk membatasi file apa saja yang diperbolehkan diupload, max untuk maksimal besar filenya
    		'user_id' => 'required',
    		'tahun' => 'required'
    	]);

        $poster = $request->poster;
        $new_poster = time() . '-' . $poster->getClientOriginalName(); // rename nama file poster
 
        profil::create([   // profil disini disebut karena menggunakan model profil.php
    		'judul' => $request->judul,
    		'ringkasan' => $request->ringkasan,
    		'poster' => $new_poster, // nama baru file poster disimpan di tabel database
    		'user_id' => $request->user_id,
    		'tahun' => $request->tahun
    	]);

        $poster->move('poster/', $new_poster); // menyimpan file yang diupload user pada alamat yang ditentukan, yaitu pada folder public/poster
 
    	return redirect('/profil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$profil = Profil::find($id);
        $tema = Tema::where('user_id',$id)->get(); // Menghitung Jumlah Tema yang dibuat user yang login
        $users = User::find($id); // mengambil data dari tabel users juga, sedangkan pengecekkan user_id pada tabel profil dan id pada tabel users dilakukan di edit.blade.php
        $profil = Profil::where('user_id',$id)->first(); // mengambil data dari tabel users juga, sedangkan pengecekkan user_id pada tabel profil dan id pada tabel users dilakukan di edit.blade.php
        $temanya = Tema::where('user_id',$id)->count(); // Menghitung Jumlah Tema yang dibuat user yang login
        $komentarnya = Komentar::where('user_id',$id)->count(); // Menghitung Jumlah Komentar yang dibuat user yang login
        $likesnya = Likes::where('user_id',$id)->count(); // Menghitung Jumlah Likes yang dibuat user yang login
        return view('profil.show', compact('users','profil','temanya','komentarnya','likesnya','tema'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profil = Profil::find($id);
        $id_user = Auth::id();  // mengammbil id user melalui Auth

        $temaku = Tema::where('user_id',$id_user)->count(); // Menghitung Jumlah Tema yang dibuat user yang login
        $komentarku = Komentar::where('user_id',$id_user)->count(); // Menghitung Jumlah Komentar yang dibuat user yang login
        $likesku = Likes::where('user_id',$id_user)->count(); // Menghitung Jumlah Likes yang dibuat user yang login
        return view('profil.edit', compact('profil','temaku','komentarku','likesku'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'name' => 'required',
    		'email' => 'required',
    		'foto_profil' => 'mimes:jpeg,png,jpg|max:2200', // mimes ini untuk membatasi file apa saja yang diperbolehkan diupload, max untuk maksimal besar filenya
    		'alamat' => 'required',
    		'umur' => 'required'
    	]);


        // untuk menentukan apakah dapat id atau tidak, jika tidak dapat id maka akan fail, namun jika dapat id maka akan sukses
        $profil = Profil::findorfail($id); 

        // Pengkondisian apakah file poster diganti atau tidak
        if ($request->has('foto_profil')) {  // jika poster diganti (user upload file baru)
            $path = 'images/foto_profil/';
           
            if($profil->foto_profil <> "null"){
                File::delete($path . $profil->foto_profil);
            }
                
            
            $foto_profil = $request->file('foto_profil');

            
            $new_foto = time() . '-' . $foto_profil->getClientOriginalName(); // rename nama file poster
            //dd($new_foto);
            $foto_profil->move($path, $new_foto); // menyimpan file yang diupload user pada alamat yang ditentukan, yaitu pada folder public/poster
            $profil_data = [
            'alamat' => $request->alamat,
    		'umur' => $request->umur,
    		'foto_profil' => $new_foto // nama baru file poster disimpan di tabel database
    	
            ];
        } else { // jika file postertidak diganti
            $profil_data = [
                'alamat' => $request->alamat,
    		    'umur' => $request->umur
                ];
    
        }
        //  dd($profil_data);

        //$user = Auth::user();
        //$user->profil()->associate($profil_data);
        //$user->save();
        
        
        //$profil->update($profil_data);  // update ke database

        //Update pada tabel user
        //$user = Auth::user();
        //$user->profil()->save($profile);

        $profil->update($profil_data);

        $user = User::findorfail($profil->user_id); 
        $user_data = [
            'name' => $request->name,
            'email' => $request->email
        ];
        $user->update($user_data);

        return redirect('/profil')->with('success','Berhasil disimpan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profil = Profil::findorfail($id);
        $profil->delete();

        $path = 'poster/';
        File::delete($path.$profil->poster);
        return redirect('/profil');
    }
}
