<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Tema; // Dipanggil karena menggunakan model Tema (Eloquent ORM), jangan salah penggunaan 'slash' (\) bukan (/)
use App\Komentar; // Dipanggil karena menggunakan model Komentar (Eloquent ORM), jangan salah penggunaan 'slash' (\) bukan (/)
use File; // Digunakan jika ada pengalamatan file
Use DB;
//Use Auth;
use App\Likes; // Dipanggil karena menggunakan model Tema (Eloquent ORM), jangan salah penggunaan 'slash' (\) bukan (/)

class TemaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth')->except('show');

    }

    public function index()
    {
        $id = Auth::id();  // mengammbil id user melalui Auth
        $tema = Tema::where('user_id', $id)->get(); // mengambil semua tema yang dimiliki Auth id User
        //$komentar = komentar::where('tema_id',$id)->count();
        $komentar = DB::table('komentar')
                ->selectRaw('count(id) as number_of_komentar, tema_id')
                ->groupBy('tema_id')
                ->get();
        return view('tema.index', compact('tema', 'komentar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = DB::table('kategori')->get();
        return view('tema.create', compact('kategori')); //untuk menuju ke form create/tambah data tema tapi sambil mengambil isi tabel kategori
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'judul' => 'required',
    		'deskripsi' => 'required',
    		'foto' => 'required|mimes:jpeg,png,jpg|max:2200', // mimes ini untuk membatasi file apa saja yang diperbolehkan diupload, max untuk maksimal besar filenya
    		'user_id' => 'required',
    		'kategori_id' => 'required',
    		'status_public' => 'required'
    	]);

        $foto = $request->foto;
        $new_foto = time() . '-' . $foto->getClientOriginalName(); // rename nama file foto
 
        Tema::create([   // Tema disini disebut karena menggunakan model Tema.php
    		'judul' => $request->judul,
    		'deskripsi' => $request->deskripsi,
    		'foto' => $new_foto, // nama baru file foto disimpan di tabel database
    		'user_id' => $request->user_id,
    		'kategori_id' => $request->kategori_id,
    		'status_public' => $request->status_public
    	]);

        $foto->move('images/', $new_foto); // menyimpan file yang diupload user pada alamat yang ditentukan, yaitu pada folder public/images
 
    	return redirect('/tema');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $id_user = Auth::id();  // mengammbil id user melalui Auth
        $tema = Tema::find($id);
        $komentar = Komentar::where('tema_id',$id)->count();
        $jml_likes = Likes::where('tema_id',$id)->count();
        $likesku = Likes::where('tema_id',$id)
                        ->where('user_id',$id_user)
                        ->count();
        $komen = Komentar::where('tema_id',$id)->get();
        $kategori = DB::table('kategori')->get(); // mengambil data dari tabel kategori juga, sedangkan pengecekkan kategori_id pada tabel tema dan id pada tabel kategori dilakukan di edit.blade.php
        return view('tema.show', compact('tema','kategori','komentar','komen','jml_likes','likesku'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tema = Tema::find($id);
        $kategori = DB::table('kategori')->get(); // mengambil data dari tabel kategori juga, sedangkan pengecekkan kategori_id pada tabel tema dan id pada tabel kategori dilakukan di edit.blade.php
        $komentar = Komentar::find($id);
        return view('tema.edit', compact('tema','kategori','komentar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'judul' => 'required',
    		'deskripsi' => 'required',
    		'foto' => 'mimes:jpeg,png,jpg|max:2200', // mimes ini untuk membatasi file apa saja yang diperbolehkan diupload, max untuk maksimal besar filenya
    		'user_id' => 'required',
    		'kategori_id' => 'required',
    		'status_public' => 'required'
    	]);
        // untuk menentukan apakah dapat id atau tidak, jika tidak dapat id maka akan fail, namun jika dapat id maka akan sukses
        $tema = Tema::findorfail($id); 

        // Pengkondisian apakah file foto diganti atau tidak
        if ($request->has('foto')) {  // jika foto diganti (user upload file baru)
            $path = 'images/';
            File::delete($path.$tema->foto);
            $foto = $request->foto;
            $new_foto = time() . '-' . $foto->getClientOriginalName(); // rename nama file foto
            $foto->move('images/', $new_foto); // menyimpan file yang diupload user pada alamat yang ditentukan, yaitu pada folder public/foto
            $tema_data = [
            'judul' => $request->judul,
    		'deskripsi' => $request->deskripsi,
    		'foto' => $new_foto, // nama baru file foto disimpan di tabel database
    		'kategori_id' => $request->kategori_id,
    		'status_public' => $request->status_public
            ];
        } else { // jika file fototidak diganti
            $tema_data = [
                'judul' => $request->judul,
                'deskripsi' => $request->deskripsi,
                'kategori_id' => $request->kategori_id,
                'status_public' => $request->status_public
                ];
        }
        $tema->update($tema_data);  // update ke database
        return redirect('/tema');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tema = Tema::findorfail($id);
        $tema->delete();

        $path = 'images/';
        File::delete($path.$tema->foto);
        return redirect('/tema');
    }
}
