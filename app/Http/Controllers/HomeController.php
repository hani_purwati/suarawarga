<?php

namespace App\Http\Controllers;
use App\Tema; // Dipanggil karena menggunakan model Profil (Eloquent ORM), jangan salah penggunaan 'slash' (\) bukan (/)
use File; // Digunakan jika ada pengalamatan file
use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tema = Tema::where('status_public', '1')->get();
        $komentar = DB::table('komentar')
                ->selectRaw('count(id) as number_of_komentar, tema_id')
                ->groupBy('tema_id')
                ->get();
        return view('home', compact('tema','komentar'));
    }
}
