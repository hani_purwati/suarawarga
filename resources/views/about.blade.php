@extends('layouts.master')

@section('title')
    @auth
    Selamat Datang  {{ Auth::user()->name }}
    @endauth
    @guest
        Anda Belum Login
    @endguest
@endsection
@section('subtitle')
    Ini adalah gambaran Project <b>Suara Warga</b>
@endsection

@section('isi')
    <h2> <b>Final Project </b></h2>

    <h4> Kelompok 22 </h4>
    <p> Terdiri dari 3 anggota, yaitu : </p>
<ul>
    <li> Rudi Nugroho C. </li>
    <li> Hani Purwati H. </li>
    <li> Roby Wijonarko P. </li>
</ul>
    <h4> Tema Project </h4>

    <p> <b>Suara Warga</b>, merupakan media untuk warga menceritakan, melaporkan, atau mengadukan berbagai hal di lingkungannya. Setiap warga dapat saling berkomentar, dan topik pembicaraan yang mendapat banyak komentar terbaru menjadi trending topik.
    </p>

    <h4> Link Penting </h4>

    <p> Link Demo Aplikasi : </p>
    <p> Link Deploy (optional) : </p>
@endsection
