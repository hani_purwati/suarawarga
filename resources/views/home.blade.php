@extends('layouts.master')

@section('title')
@auth
Selamat Datang  {{ Auth::user()->name }}
@endauth
@guest
    Anda Belum Login
@endguest
@endsection
@section('subtitle')
    Apa sih fokus kamu hari ini?  
    @auth
        <a href="/tema/create" class="btn btn-primary">Tambah Topik Yuk !</a>
    @endauth
@endsection
@section('isi')
    <div class="row">
        @foreach ($tema as $item)
        <div class="col-md-3">
            <div class="card" style="width: 25rem">
                <img src="{{ asset('images/'.$item->foto)}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title"><b>{{$item->judul}}</b></h5>
                    <p class="card-text">{{ Str::limit($item->deskripsi, 50)}}</p>
                    <p class="card-text">Jumlah Komentar:  
                        @foreach ($komentar as $komen)
                            @if ($komen->tema_id == $item->id)
                                {{$komen->number_of_komentar}}                                    
                            @endif                                
                        @endforeach

                    </p>
                <a href="/tema/{{$item->id}}" class="btn btn-primary" >Detail</a>
                </div>
            </div> 
        </div>       
        @endforeach
    </div>
@endsection