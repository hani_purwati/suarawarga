@extends('layouts.master')

@section('title')
    Selamat Datang di Media Sosial Suara Warga
@endsection
@section('subtitle')
    Jenis Kategori
@endsection

@section('isi')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Tambahkan Jenis Kategori</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="/kategori/{{$kategori->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label for="nama_kategori">Isikan Nama Kategori</label>
                <input type="text" class="form-control" name="nama_kategori" id="nama_kategori"  value="{{$kategori->nama_kategori}}">
                @error('nama_kategori')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="keterangan">Keterangan</label>
                <textarea class="form-control" name="keterangan" id="keterangan" >{{$kategori->keterangan}}</textarea>
                @error('keterangan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
        </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Simpan</button>
        <a href="/kategori" class="btn btn-danger">Batal</a>
      </div>
    </form>
  </div>
@endsection