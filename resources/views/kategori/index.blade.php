@extends('layouts.master')

@section('title')
    Selamat Datang di Media Sosial Suara Warga
@endsection
@section('subtitle')
    Jenis Kategori
@endsection
@push('script')
<script src="{{ asset('/AdminPanel/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('/AdminPanel/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush
@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.0/datatables.min.css"/>
@endpush
@section('isi')
<a href="/kategori/create" class="btn btn-primary">Tambah</a> <br>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Jenis Kategori</th>
                <th scope="col">Keterangan</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($kategori as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                            <td>{{$value->nama_kategori}}</td>
                            <td>{{$value->keterangan}}</td>
                            <td>
                        <!--    <a href="/kategori/{{$value->id}}" class="btn btn-info">Show</a> di off karena sudah jelas semua field di index -->
                            <a href="/kategori/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/kategori/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
@endsection