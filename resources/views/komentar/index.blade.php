@extends('layouts.master')

@section('title')
    Selamat Datang di Media Sosial Suara Warga
@endsection
@section('subtitle')
    Jenis komentar
@endsection
@push('script')
<script src="{{ asset('/AdminPanel/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('/AdminPanel/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush
@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.0/datatables.min.css"/>
@endpush
@section('isi')
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Komentarku</th>
                <th scope="col">Lihat Tema yang Dikomentari</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($komenku as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                            <td>{{$value->komen}}</td>
                            <td>
                        <a href="/tema/{{$value->tema_id}}" class="btn btn-info">Show</a> 
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
@endsection