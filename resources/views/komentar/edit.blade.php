@extends('layouts.master')

@section('title')
    Selamat Datang {{ Auth::user()->name }}
@endsection
@section('subtitle')
    Apa sih topik menarik hari ini?? 

@endsection

@section('isi')
        <div class="form-group">
            <h4><b>{{$tema->judul}}</b></h4>
        </div>
        <div class="form-group">
            <p>{{$tema->deskripsi}}</p>
        </div>
        <form action="/komentar" method="POST" enctype="multipart/form-data"> <!-- multipart/form-data ini ditambahkan di form karena poster diinput berupa file-->
            @csrf
            <div class="form-group">
            <label for="komentar">Komentar</label>
            <input type="hidden" class="form-control" name="user_id" id="user_id" value="{{ Auth::user()->id }}"> <!-- user id ini adalah user id yang berkomentar, bukan user id yang membuat komentar  -->
            <input type="hidden" class="form-control" name="tema_id" id="user_id" value="00001{{$tema->id}}"> <!-- 00001 menandakan parent dari tema, kalau parent dari komentar (reply komentar) 00002-->
            <textarea class="form-control" name="komen" id="komen" rows="5"></textarea>
            @error('komen')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="foto">Foto</label>
            <div class="input-group">
                <div class="custom-file">
                    <input type="file" class="form-control" name="foto" id="foto">
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Tambahkan Komentar</button>
    </form>
@endsection
