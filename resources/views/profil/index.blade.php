@extends('layouts.master')

@section('title')
    Selamat Datang {{ Auth::user()->name }}
@endsection
@section('subtitle')
    Apa sih fokus kamu hari ini?
@endsection

@section('isi')
<!-- Mulai Tampilan Profil -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-3">
              <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{ asset('images/foto_profil/'. Auth::user()->profil->foto_profil)}}"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>

                <p class="text-muted text-center">Software Engineer</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Postingan yang dibuat</b> <a class="float-right">{{$temaku}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Komentar yang dibuat</b> <a class="float-right">{{$komentarku}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Menyukai Postingan</b> <a class="float-right">{{$likesku}}</a>
                  </li>
                </ul>

                <a href="/tema" class="btn btn-primary btn-block"><b>Lihat Postinganku</b></a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">About Me</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
                @endif
                <strong><i class="fas fa-book mr-1"></i>Nama</strong>
                    <p class="text-muted">{{$profil->user->name}}</p>
                <hr>

                <strong><i class="fas fa-map-marker-alt mr-1"></i>Alamat</strong>
                <p class="text-muted">{{$profil->alamat}}</p>
                <hr>

                <strong><i class="fas fa-email-alt mr-1"></i> Email</strong>
                <p class="text-muted">{{$profil->user->email}}</p>
                <hr>

                <strong><i class="far fa-file-alt mr-1"></i> Umur</strong>
                    <p class="text-muted">{{$profil->umur}}</p>
                <hr>
                
                <strong><i class="fas fa-map-marker-alt mr-1"></i>Foto</strong>
                
                <img src="{{ asset('images/foto_profil/'.$profil->foto_profil)}}" class="card-img-top" alt="...">
                <hr>
                
                <a href="/profil/{{$profil->id}}/edit" class="text-center">Edit Profil</a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Postingan Terbaru</a></li>
                  <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Trending Topik</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Profil User</a></li>
                  <li class="nav-item"><a class="nav-link" href="#alluser" data-toggle="tab">Data Table User</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <!-- Postingan Terbaru--> 
                    @include('partial.post')
                    <!-- Post -->

                    <!-- /.post -->
                  </div>
                  <!-- /.tab-pane -->

                  <div class="tab-pane" id="timeline">
                    <!-- The timeline DIMULAI -->
                    @include('partial.trending')
                  </div>
                  <!-- /.tab-pane -->

                  <div class="tab-pane" id="settings">
                    @include('partial.folluser')
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="alluser">
                    @include('partial.alluser')
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- Akhir Tampilan Profil -->
@endsection