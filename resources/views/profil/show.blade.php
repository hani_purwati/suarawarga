@extends('layouts.master')

@section('title')
    Selamat Datang {{ Auth::user()->name }}
@endsection
@section('subtitle')
    Ini adalah Biodata User {{$users->id}}
@endsection

@section('isi')
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">
        <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle"
                    src="{{ asset('images/foto_profil/'. $profil->foto_profil)}}"
                    alt="User profile picture">
              </div>

              <h3 class="profile-username text-center">{{$users->name}}</h3>

              <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                  <b>Postingan yang dibuat</b> <a class="float-right">{{$temanya}}</a>
                </li>
                <li class="list-group-item">
                  <b>Komentar yang dibuat</b> <a class="float-right">{{$komentarnya}}</a>
                </li>
                <li class="list-group-item">
                  <b>Menyukai Postingan</b> <a class="float-right">{{$likesnya}}</a>
                </li>
              </ul>
            </div>
            <!-- /.card-body -->
          </div>


            <!-- About Me Box -->
            <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">About User</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @if(session('success'))
                <div class="alert alert-success">
                    {{session('success')}}
                </div>
                @endif
                <strong><i class="fas fa-book mr-1"></i>Nama</strong>
                    <p class="text-muted">{{$users->name}}</p>
                <hr>

                <strong><i class="fas fa-map-marker-alt mr-1"></i>Alamat</strong>
                <p class="text-muted">{{$profil->alamat}}</p>
                <hr>

                <strong><i class="fas fa-email-alt mr-1"></i> Email</strong>
                <p class="text-muted">{{$users->email}}</p>
                <hr>

                <strong><i class="far fa-file-alt mr-1"></i> Umur</strong>
                    <p class="text-muted">{{$profil->umur}}</p>
                <hr>
                
                <strong><i class="fas fa-map-marker-alt mr-1"></i>Foto</strong>
                
                <img src="{{ asset('images/foto_profil/'.$profil->foto_profil)}}" class="card-img-top" alt="...">
                <hr>
            </div>
            <!-- /.card-body -->
            </div>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</div><!-- /.container-fluid -->
</section>

@endsection
