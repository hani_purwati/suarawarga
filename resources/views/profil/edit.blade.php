@extends('layouts.master')

@section('title')
    Selamat Datang {{ Auth::user()->name }}
@endsection
@section('subtitle')
    Apa sih fokus kamu hari ini?
@endsection

@section('isi')
<!-- Mulai Tampilan Profil -->
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-3">
              <!-- Profile Image -->
              <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                  <div class="text-center">
                    <img class="profile-user-img img-fluid img-circle"
                         src="{{ asset('images/foto_profil/'. Auth::user()->profil->foto_profil)}}"
                         alt="User profile picture">
                  </div>
  
                  <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>
  
                  <p class="text-muted text-center">Software Engineer</p>
  
                  <ul class="list-group list-group-unbordered mb-3">
                    <li class="list-group-item">
                      <b>Postingan yang dibuat</b> <a class="float-right">{{$temaku}}</a>
                    </li>
                    <li class="list-group-item">
                      <b>Komentar yang dibuat</b> <a class="float-right">{{$komentarku}}</a>
                    </li>
                    <li class="list-group-item">
                      <b>Menyukai Postingan</b> <a class="float-right">{{$likesku}}</a>
                    </li>
                  </ul>
  
                  <a href="/tema" class="btn btn-primary btn-block"><b>Lihat Postinganku</b></a>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">About Me</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                
                <strong><i class="fas fa-book mr-1"></i>Nama</strong>
                    <p class="text-muted">{{$profil->user->name}}</p>
                <hr>

                <strong><i class="fas fa-map-marker-alt mr-1"></i>Alamat</strong>
                <p class="text-muted">{{$profil->alamat}}</p>
                <hr>

                <strong><i class="fas fa-email-alt mr-1"></i> Email</strong>
                <p class="text-muted">{{$profil->user->email}}</p>
                <hr>

                <strong><i class="far fa-file-alt mr-1"></i> Umur</strong>
                    <p class="text-muted">{{$profil->umur}}</p>
                <hr>
                
                <strong><i class="fas fa-map-marker-alt mr-1"></i>Foto</strong>
                <p class="text-muted">{{$profil->foto_profil}}</p>
                <img src="{{ asset('images/foto_profil/'.$profil->foto_profil)}}" class="card-img-top" alt="..." style="width: 15rem">
                <hr>
                
                <a href="/profil/{{$profil->id}}/edit" class="text-center">Edit Profil</a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                EDIT PROFIL
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  
                    <form action="/profil/{{$profil->id}}" enctype="multipart/form-data" method="POST">
                        @csrf
                        @method("PUT")
                        <div class="input-group mb-3">
                        <input type="text" class="form-control" value="{{old('name',$profil->user->name)}}" placeholder="Nama Lengkap" name="name" id="name">
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <div class="input-group-append">
                                <div class="input-group-text">
                                <span class="fas fa-user"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                        <input type="email" class="form-control" value="{{old('email',$profil->user->email)}}" placeholder="Email" name="email" id="email">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                            </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="number" class="form-control" value="{{old('umur',$profil->umur)}}" placeholder="Masukkan Umur" name="umur" id="umur">
                            @error('umur')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror      
                            <div class="input-group-append">
                                    <div class="input-group-text">
                                    <span class="fas fa-user"></span>
                                    </div>
                                </div>
                            </div>
                                <div class="input-group mb-3">
                                    <textarea class="form-control" placeholder="Masukkan Alamat" name="alamat" id="alamat">{{old('alamat',$profil->alamat)}}</textarea>
                                    @error('alamat')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror      
                                    <div class="input-group-append">
                                            <div class="input-group-text">
                                            <span class="fas fa-user"></span>
                                            </div>
                                        </div>
                                    </div>
                                <div class="input-group mb-3">


                                <div class="form-group">
                                    <label for="foto_profil">Foto Profil</label>
                                    <div class="input-group">
                                    <img src="{{ asset('images/foto_profil/'.$profil->foto_profil)}}" class="card-img-top" alt="..." style="width: 10rem">
                                        <div class="custom-file">
                                            <input type="file" class="form-control" name="foto_profil" id="foto_profil">
                                        </div>
                                    </div>
                                    @error('foto_profil')
                                        <div class="alert alert-danger">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                        
                            <!-- /.col -->
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
            <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- Akhir Tampilan Profil -->
@endsection