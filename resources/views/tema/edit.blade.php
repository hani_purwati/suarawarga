@extends('layouts.master')

@section('title')
    Selamat Datang {{ Auth::user()->name }}
@endsection
@section('subtitle')
    Apa sih topik menarik hari ini?? 
@endsection

@section('isi')
<form action="/tema/{{$tema->id}}" method="POST" enctype="multipart/form-data"> <!-- multipart/form-data ini ditambahkan di form karena poster diinput berupa file-->
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="judul">Judul Topik Pembicaraan</label>
            <input type="text" class="form-control" name="judul" id="title" value="{{$tema->judul}}" maxlength="45">
            @error('judul')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="deskripsi">Deskripsi</label><input type="hidden" class="form-control" name="user_id" id="user_id" value="{{ Auth::user()->id }}">
            <textarea class="form-control" name="deskripsi" id="deskripsi" rows="5">{{$tema->deskripsi}}</textarea>
            @error('deskripsi')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="status_public">Status Public {{$tema->status_public}}</label>
            <select class="custom-select form-control-border" id="status_public" name="status_public">
                @if ($tema->status_public == "1")
                    <option value="{{$tema->status_public}}" selected>Public</option>
                    <option value="2">Private</option>
                @else 
                    <option value="1">Public</option>
                    <option value="{{$tema->status_public}}" selected>Private</option>
                @endif
            </select>
          </div>
        <div class="form-group">
            <label for="kategori_id">Kategori Topik (Tags)</label>
            <select name="kategori_id" id="kategori_id" class="form-control">
                <option value="" disabled>Pilih Kategori</option>
            @foreach ($kategori as $item)
                @if ($item->id === $tema->kategori_id)
                    <option value="{{$item->id}}" selected>{{$item->nama_kategori}}</option>
                @else 
                    <option value="{{$item->id}}">{{$item->nama_kategori}}</option>
                @endif
            @endforeach
            </select>
            @error('kategori_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="foto">Foto</label>
            <div class="input-group">
                <div class="custom-file">
                    <input type="file" class="form-control" name="foto" id="foto">
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
        <a href="/tema" class="btn btn-danger">Batal</a>
    </form>
@endsection
