@extends('layouts.master')

@section('title')
    Selamat Datang {{ Auth::user()->name }}
@endsection
@section('subtitle')
    Apa sih fokus kamu hari ini? <a href="/tema/create" class="btn btn-primary">Tambah Topik Yuk !</a>
@endsection
@push('script')
<script src="{{ asset('/AdminPanel/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('/AdminPanel/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#data_tema").DataTable();
  });
</script>
@endpush
@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.0/datatables.min.css"/>
@endpush
@section('isi')
    <div class="row" id="data_tema">
            @foreach ($tema as $item)
            <div class="col-md-3">
                <div class="card" style="width: 25rem">
                        <img src="{{ asset('images/'.$item->foto)}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title"><b>{{$item->judul}}</b></h5>
                        <p class="card-text">{{ Str::limit($item->deskripsi, 50)}}</p>
                        <p class="card-text">Jumlah Komentar:  
                            @foreach ($komentar as $komen)
                                @if ($komen->tema_id == $item->id)
                                    {{$komen->number_of_komentar}}  
                                @endif                                
                            @endforeach

                        </p>
                        <a href="/tema/{{$item->id}}" class="btn btn-primary" >Detail</a>
                    @if ($item->user_id === Auth::user()->id)
                        <a href="/tema/{{$item->id}}/edit" class="btn btn-warning" >Edit</a>
                        <form action="/tema/{{$item->id}}" method="POST">
                            @csrf
                            @method('delete')
                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                    @endif
                    </div>
                </div>        
            </div>
            @endforeach
    </div>
@endsection