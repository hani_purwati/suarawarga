@extends('layouts.master')
@section('title')
    @auth
    Selamat Datang  {{ Auth::user()->name }}
    @endauth
    @guest
        Anda Belum Login
    @endguest
@endsection
@section('subtitle')
    Detail Tema {{$tema->judul}}
@endsection
@section('isi')  <!--  Menampilkan Detail Temaku     -->
    <a href="/" class="btn btn-danger">Kembali ke List Utama</a>
    @auth
    <a href="/tema" class="btn btn-warning">Kembali ke List Temaku</a>
    @endauth
<div class="row">
    <div class="col-xl-3">
        <img src="{{ asset('images/'.$tema->foto)}}" class="my-3" alt="..." style="width: 25rem">
    </div>
    <div class="col-xl-9">
        <p></p>
        <h4> {{$tema->judul}}</h4>
        @foreach ($kategori as $item)
        @if ($item->id === $tema->kategori_id)
            kategori : {{$item->nama_kategori}}
        @endif
        @endforeach
        <p>{{$tema->deskripsi}}</p>
    </div>
    <div class="col-xl-2">
        Jumlah Komentar : {{$komentar}}
    </div>
    <div class="col-xl-2">
        Jumlah Likes : {{$jml_likes}}
    </div>
    <div class="col-xl-3">
        <a href="/komentar/{{$tema->id}}/create" class="btn btn-primary" >Beri komentar</a>
    </div>
    <div class="col-xl-5">
    @if ($likesku == 0)
    <form action="/likes" method="POST">
        @csrf
        <input type="hidden" class="form-control" name="tema_id" id="tema_id" value="{{$tema->id}}">
        <button type="submit" class="btn btn-primary">Sukai</button>
    </form>
    @else
    <a href="#" class="btn btn-warning" >Anda telah menyukai Tema ini</a>
    @endif

    </div>
</div>
<br>
@auth
<!--  Menampilkan Komentar Temaku     -->
<div class="row">
    @forelse ($komen as $key=>$value)
    <div class="col-md-1">{{$key + 1}}</div>
    <div class="col-md-9">{{$value->komen}}</div>
    <div class="col-md-2">
        @if ($value->user_id === Auth::user()->id)
            <form action="/komentar/{{$value->id}}" method="POST">
                @csrf
                @method('DELETE')
                <input type="submit" class="btn btn-danger my-1" value="Hapus Komentar">
            </form>
        @endif
    </div>
    <hr/>
    @empty
        <div class="col-md-12">No data</div>
    @endforelse  
</div>            
@endauth
@endsection
