@extends('layouts.master')

@section('title')
    Selamat Datang {{ Auth::user()->name }}
@endsection
@section('subtitle')
    Apa sih topik menarik hari ini?? 

@endsection

@section('isi')
    <form action="/tema" method="POST" enctype="multipart/form-data"> <!-- multipart/form-data ini ditambahkan di form karena poster diinput berupa file-->
        @csrf
        <div class="form-group">
            <label for="judul">Judul Topik Pembicaraan</label>
            <input type="text" class="form-control" name="judul" id="title" placeholder="Masukkan Judul Topik" maxlength="45">
            @error('judul')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="deskripsi">Deskripsi</label> <input type="hidden" class="form-control" name="user_id" id="user_id" value="{{ Auth::user()->id }}">
            <textarea class="form-control" name="deskripsi" id="deskripsi" rows="5"></textarea>
            @error('deskripsi')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="status_public">Status Public</label>
            <select class="custom-select form-control-border" id="status_public" name="status_public">
              <option value="1">Public</option>
              <option value="2">Private</option>
            </select>
          </div>
        <div class="form-group">
            <label for="kategori_id">Kategori Topik (Tags)</label>
            <select name="kategori_id" id="kategori_id" class="form-control">
                <option value="" disabled>Pilih Kategori</option>
                @foreach ($kategori as $item)
                    <option value="{{$item->id}}"> {{$item->nama_kategori}} </option>
                @endforeach
            </select>
            @error('kategori_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="foto">Foto</label>
            <div class="input-group">
                <div class="custom-file">
                    <input type="file" class="form-control" name="foto" id="foto">
                </div>
            </div>
            @error('foto')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
        <a href="/" class="btn btn-danger">Batal</a>
    </form>
@endsection
