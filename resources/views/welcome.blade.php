@extends('layouts.master')

@section('title')
    @auth
    Selamat Datang  {{ Auth::user()->name }}
    @endauth
    @guest
        Anda Belum Login
    @endguest
@endsection
@section('subtitle')
    Ini adalah gambaran ERD Project Suara Warga
@endsection

@section('isi')
<img src={{ asset('/images/ERD.png')}}>
@endsection
