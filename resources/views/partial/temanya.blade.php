<!--   Menampilkan Postingan Terbaru       -->
<div class="post">
    <div class="user-block">
      @foreach ($tema as $item)
        <img class="img-circle img-bordered-sm" src="{{ asset('images//'. $item->foto)}}" alt="user image">
        <span class="username">
          <a href="/tema/{{$item->id}}">
                  {{$item->judul}}
          </a>
        <!--  <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a> -->
        </span>
      @endforeach            
    <span class="description">{{$item->created_at}}</span>
    </div>
    <!-- /.user-block -->
    <p>{{ Str::limit($item->deskripsi, 100)}}</p>

    <p>
      <a href="/tema/{{$item->id}}" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i>Lihat</a>
    </p>
  </div>
