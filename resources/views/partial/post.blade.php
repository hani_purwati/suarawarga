@forelse ($semua_tema as $key=>$value)
<!--   Menampilkan Postingan Terbaru       -->
<div class="post">
    <div class="user-block">
      @foreach ($semua_user as $item)
      @if ($item->id === $value->user_id)
        <img class="img-circle img-bordered-sm" src="{{ asset('images/foto_profil/'. $item->foto_profil)}}" alt="user image">
        <span class="username">
          <a href="/profil/{{$value->user_id}}"  >
                  {{$item->name}}
          </a>
        <!--  <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a> -->
        </span>
      @endif
      @endforeach            
    <span class="description">{{$value->created_at}}</span>
    </div>
    <!-- /.user-block -->
    <p>{{ Str::limit($value->deskripsi, 100)}}</p>

    <p>
      <a href="/tema/{{$value->id}}" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i>Lihat</a>
    <!--  <a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Sukai</a> -->
      <span class="float-right">
        <a href="#" class="link-black text-sm">
          <i class="far fa-comments mr-1"></i> Jumlah Komentar :
          @foreach ($komentar as $komen)
            @if ($komen->tema_id == $value->id)
                {{$komen->number_of_komentar}}                 
            @endif                                
          @endforeach
        </a>
      </span>
    </p>
  </div>
  @empty

  @endforelse              
