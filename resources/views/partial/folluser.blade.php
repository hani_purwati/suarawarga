<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama User</th>
        <th scope="col">Alamat</th>
        <th scope="col">Foto</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($semua_user as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                    <td>{{$value->name}}</td>
                    <td>{{$value->alamat}}</td>
                    <td><img class="profile-user-img img-fluid img-circle"
                        src="{{ asset('images/foto_profil/'. $value->foto_profil)}}"
                        alt="User profile picture">
                    </td>
                    <td>
                    <a href="/profil/{{$value->id}}" class="btn btn-primary">Detail</a>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>
        @endforelse
    </tbody>
</table>
