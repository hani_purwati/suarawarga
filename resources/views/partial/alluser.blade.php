@push('tuser')
<script src="{{ asset('/AdminPanel/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('/AdminPanel/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#tbuser").DataTable();
  });
</script>
@endpush
@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.0/datatables.min.css"/>
@endpush

<table id="tbuser" class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama User</th>
        <th scope="col">E-Mail</th>
        <th scope="col">Pertama jadi Member</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($alluser as $key=>$value)
            <tr>
                <th>{{$key + 1}}</th>
                <td>{{$value->name}}</td>
                <td>{{$value->email}}</td>
                <td>{{$value->created_at}}</td>
                <td><a href="/profil/{{$value->id}}" class="btn btn-primary">Detail</a></td>
            </tr>
        @empty
            <tr colspan="5">
                <td>No data</td>
            </tr>
        @endforelse
    </tbody>
</table>
