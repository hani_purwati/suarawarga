<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
      <img src="{{ asset('AdminPanel/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Suara Warga</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user, ada @ auth karena untuk memisahkan kapan dapan diakses dan akapn tidak muncul namanya -->
      @auth
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">

            <img src="{{ asset('images/foto_profil/'. Auth::user()->profil->foto_profil)}}" class="img-circle elevation-2" alt="User Image"> <!-- Nanti disini adalah photo user yang login -->
          </div>
          <div class="info">
            <a href="/profil" class="d-block">{{ Auth::user()->name }}</a>
          </div>
        </div>

        <!-- SidebarSearch Form -->
        <div class="form-inline">
          <div class="input-group" data-widget="sidebar-search">
            <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
              <button class="btn btn-sidebar">
                <i class="fas fa-search fa-fw"></i>
              </button>
            </div>
          </div>
        </div>
      @endauth

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/erd" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>ERD</p>
                </a>
              </li>
            </ul>
          </li>
          @guest
          <li class="nav-item">
            <a href="/login" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Login</p>
            </a>
          </li>
          @endguest
          @auth  <!-- Yang hanya akan muncul kalau login  -->
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Temaku
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/tema" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Semua Temaku</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/komentar" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Komentarku</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="/" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Setting
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/kategori" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kategori</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/profil" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p> Profil</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/profil/{{Auth::user()->profil->id}}/edit" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p> Edit Profil</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/password" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Ubah Password</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
              onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                <i class="nav-icon far fa-image"></i>
                {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
          @endauth
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
